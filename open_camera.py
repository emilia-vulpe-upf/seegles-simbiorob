# This code allows opening and chosing the desired camera
# The command is: python open_camera.py -n N where N is the camera number

import numpy as np
import cv2
import argparse

# Addition of an argument that allows choosing among the cameras
parser = argparse.ArgumentParser()
parser.add_argument('-n', action='store', help ="choose a camera", type = int, default=0)
args = parser.parse_args()
cam = args.n

cap = cv2.VideoCapture(cam)

while(True):
	# Capture frame-by-frame
	ret, frame = cap.read()

	# The operation on the frame come here
	#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	# Display the resulting frame
#	cv2.imshow('frame')
	cv2.imshow('frame', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

