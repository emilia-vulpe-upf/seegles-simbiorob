import time
import cv2
import cv2.aruco as A
import numpy as np
import argparse
import traceback
import sys
import pickle 

def open_camara(cid):
  cap = cv2.VideoCapture(cid)
  return cap

def init_aruco():
  dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_1000)
  board = cv2.aruco.CharucoBoard_create(8,5,.025,.0125,dictionary)
  img = board.draw((200*3,200*3))

  #Dump the calibration board to a file
  cv2.imwrite('charuco_board.png',img)

  return board, dictionary

if __name__ == "__main__":

  parser = argparse.ArgumentParser()
  parser.add_argument('-c', help ="choose a camera", type = int, default = 0)
  parser.add_argument('-n', help ="Number of frames", type = int, default = 100)
  args = parser.parse_args()
  cam = args.c
  frames = args.n

  board, dictionary = init_aruco()

  cap = open_camara(cam)

  #Start capturing images for calibration
  # Addition of an argument that allows choosing among the cameras

  print "Please adjust board position to ensure that it fits completely in camera"
  print "Strike 'c' to start calibration"
  while(True):
    ret, frame = cap.read()		
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) == ord('c'):
      break

  print "Starting %d frames acquisition..."%frames
  allCorners = []
  allIds = []
  for i in range(frames):
    ret, frame = cap.read()
    res = cv2.aruco.detectMarkers(frame, dictionary)

    if len(res[0])>0:
      res2 = cv2.aruco.interpolateCornersCharuco(res[0],res[1],frame, board)
      if (res2[1] is not None) and (res2[2] is not None) and (len(res2[1])>3):
        allCorners.append(res2[1])
        allIds.append(res2[2])

      cv2.aruco.drawDetectedMarkers(frame,res[0],res[1])

    cv2.imshow('frame',frame)
    cv2.waitKey(1)

  # Extract 1 channel shape
  imsize = frame.shape[::1][1:3]

  #Calibration fails for lots of reasons. Release the video if we do
  try:
	  rett,mtx,dist,rvecs,tvecs = cv2.aruco.calibrateCameraCharuco(allCorners, allIds,board,imsize,None,None)
	  if rett: # If calibration was successfull return value is True:
	    print "Camera mtx is", mtx
	    print "Camera dist is", dist
	    print "Camera rvecs is", rvecs
	    print "Camera tvecs is", tvecs
	    with open("camera_calibration.pickle", "wb") as fp:
	      pickle.dump((mtx,dist,rvecs,tvecs), fp)
  except:
	  print "Camera calibration failed"
	  cap.release()
	  print  sys.exc_info() #traceback.print_exc()
 
  # print cal.shape

  cap.release()
  cv2.destroyAllWindows()




