# Marker detector code 

import numpy as np
import cv2
import cv2.aruco as aruco
import pickle 
import pprint

# marker creation and saving the image to png
   # aruco_dict = aruco.Dictionary_get(aruco.DICT_7X7_250)
   # aruco_marker = aruco.drawMarker(aruco_dict, 23,200,1)

   # cv2.imwrite('Marker_dict7x7_250_23.png',aruco_marker)


# load camera calibration parameters
pkl_file = open('camera_calibration.pickle', 'rb')
data1 = pickle.load(pkl_file)
# pprint.pprint(data1)
# print data1[2][0].shape

mtx, dist, rvecs, tvecs = data1[0], data1[1], data1[2], data1[3]

print "Camera matrix is: ...""\n", mtx
print "Distance coefficients are: ...""\n", dist 
print "Rotation vectors are: ...""\n", rvecs
print "Translation vectors are: ...""\n", tvecs


cap = cv2.VideoCapture(0)
markerLength = 50

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    #print(frame.shape) #480x640
    # Our operations on the frame come here
    aruco_dict = aruco.Dictionary_get(aruco.DICT_7X7_250)
    parameters =  aruco.DetectorParameters_create()

    #print(parameters)

    '''    detectMarkers(...)
        detectMarkers(image, dictionary[, corners[, ids[, parameters[, rejectedI
        mgPoints]]]]) -> corners, ids, rejectedImgPoints
        '''
        #lists of ids and the corners beloning to each id
    corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
    print(corners)

    # Pose Estimation
    # rvecs, tvecs, objpointsbg = aruco.estimatePoseSingleMarkers(corners,0.05,)
    if ids != None: # if aruco marker detected
        rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners, markerLength, mtx, dist)
        colorFrame = aruco.drawAxis(frame, mtx, dist, rvec, tvec, 100)

        # It's working.
        # my problem was that the cellphone put black all around it. The alrogithm
        # depends very much upon finding rectangular black blobs
        # changed frame to colorFrame -> before it was gray parameter, check there is no problem here
        colorFrame = aruco.drawDetectedMarkers(colorFrame, corners)

        #print(rejectedImgPoints)
        # Display the resulting frame
        cv2.imshow('frame',colorFrame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

